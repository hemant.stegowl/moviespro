import { useState } from 'react'
import Header from './components/Header'
import MovieCard from './components/MovieCard'
import MovieSection from './components/MovieSection'
import './App.css'

function App() {
 

  return (
    <div className='py-0 h-screen'>
      <Header/>
      <MovieSection/>
      
    </div>
  )
}

export default App
