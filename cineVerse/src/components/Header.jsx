import React,{useState} from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from "react-redux";



function Header() {
//   const dispatch = useDispatch();
//   const moviesData = useSelector((state) => state.movies.moviesData);
   
//   const[inputMovies,setInputMovies]=useState('')

// const [filterMovies,setFilteredMovies]=useState([])

// const handleInput=(event)=>{
//   const input=event.target.value
//   setInputMovies(input)
//   const filteredMovies=moviesData.filter((movieObj)=>{
//     return movieObj.movie.toLowerCase().includes(input)
//   })
//   if(!input){
//     setFilteredMovies([])
//   }
//   else{
//     setFilteredMovies(filteredMovies)
//   }
// }
  return (
    <div  className="flex justify-center  items-center  py-2 border-2  ">
      
      {/* <div >
        <input className='border-2 border-black' type="text" placeholder='enter movie name' onChange={handleInput} value={inputMovies} />
      </div> */}
      <Link to={'/movies'}><h1 className="text-2xl font-bold pr-24 ">cineVerse</h1></Link>
      <Link  to={'/movies/watchList'}><h1 className='inline-block  '>My Watch List</h1></Link>
    
    </div>
  )
}

export default Header
