import React, {useState} from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { setWatchList,setAddedMovies } from "../features/moviesSlice";


function MovieCard(props) {

    const inputMovies=props.inputMovies
    const dispatch=useDispatch()
    const watchList=useSelector(state=>state.movies.watchList)
    // const [watchList,setWatchList]=useState([])
console.log('✌️watchList --->', watchList);

    const addedMovies = useSelector((state)=>state.movies.addedMovies)

    const handleWatchList=(one)=>{
        console.log('movie that i am goingto add to watch later',one)
        if (!watchList.some(movie => movie.movie === one.movie)) {
            // If the movie is not already in the watch list, add it
            const newWatchList = [
                ...watchList,
                one
            ];
            dispatch(setWatchList(newWatchList))
            // setWatchList(newWatchList);
            dispatch(setAddedMovies([...addedMovies, one.movie]));

        } else {
            // If the movie is already in the watch list, you can handle this scenario
            console.log(`${one.movie} is already in the watch list.`);
        }
    }

    return (
     <>
     {!inputMovies && 
        props.moviesData.map(one=>{
            return(
                <div className="border-2 border-black  my-2 w-1/6 rounded-md h-full	  md:mr-4 lg:mr-6">
                    <Link to={`/movies/${one.movie.replace(/\s+/g, '-')}`}>
                 <div className="border-2 border-black h-52 py-0 relative  ">
                        <img src={one.image} alt="" className="absolute inset-0 w-full h-full object-cover"/>
                    </div>
                    </Link>
                    <div className="flex  h-1/4">
                        <h1 className="w-9/12 text-lg text-center font-bold">{one.movie}</h1>
                        
                     <div >
                            {/* <button className="border-2 bg-green-200 rounded-md w-10" onClick={()=>{handleWatchList(one)}}>add</button> */}
                            <button
                                    className={`border-2 rounded-md w-10 ${
                                        addedMovies.includes(one.movie)
                                            ? 'bg-green-500 text-white'
                                            : 'bg-green-200'
                                        }`}
                                    onClick={() => handleWatchList(one)}
                                >
                                    {addedMovies.includes(one.movie) ? 'Done' : 'Add'}
                                </button>
                    </div>
                </div>
                
              </div>
            )
         })
    }

    {inputMovies && 
        props.currentSearchMovies.map(one=>{
            return(
                <div className="border-2 border-black  my-2 w-1/6 rounded-md h-full	  md:mr-4 lg:mr-6">
                    <Link to={`/movies/${one.movie.replace(/\s+/g, '-')}`}>
                 <div className="border-2 border-black h-52 py-0 relative  ">
                        <img src={one.image} alt="" className="absolute inset-0 w-full h-full object-cover"/>
                    </div>
                    </Link>
                    <div className="flex  h-1/4">
                        <h1 className="w-9/12 text-lg text-center font-bold">{one.movie}</h1>
                        
                     <div >
                            {/* <button className="border-2 bg-green-200 rounded-md w-10" onClick={()=>{handleWatchList(one)}}>add</button> */}
                            <button
                                    className={`border-2 rounded-md w-10 ${
                                        addedMovies.includes(one.movie)
                                            ? 'bg-green-500 text-white'
                                            : 'bg-green-200'
                                        }`}
                                    onClick={() => handleWatchList(one)}
                                >
                                    {addedMovies.includes(one.movie) ? 'Done' : 'Add'}
                                </button>
                    </div>
                </div>
                
              </div>
            )
         })
    }
    
     </>
       
      
    );
//   return (
    
//       <div className="border-2 border-black  my-2 w-1/6 rounded-md">
//         <div className="border-2 border-black h-3/4 py-0 relative  ">
//             <img src="https://plus.unsplash.com/premium_photo-1661849963038-7a735f000ae0?q=80&w=1830&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" alt="" className="absolute inset-0 w-full h-full object-cover"/>
//         </div>
//         <div className="flex border-2 h-1/4">
//             <h1 className="border-2 w-9/12 text-lg text-center font-bold">Movie Title</h1>
//             <div >
//                 <button className="border-2 bg-green-200 rounded-md w-10">add</button>
//             </div>
//         </div>
//       </div>
    
//   );
}

export default MovieCard;
