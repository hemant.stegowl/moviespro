import React from 'react'
import { useParams } from 'react-router-dom'
import { useDispatch, useSelector } from "react-redux";


function MovieDetails() {

  const dispatch=useDispatch();
  const moviesData=useSelector((state)=>state.movies.moviesData)
console.log('✌️moviesData --->', moviesData);

  
    const {movieTitle}=useParams()
console.log('✌️title --->', movieTitle?movieTitle:'null');
// i have the movie name that i click now i want the details of the movie rom the movieData how i have to fetch the object from movieData that have the same name as thisthen i can dispaly
    const movie=moviesData.find(movie=>movie.movie.replace(/\s+/g, '-')===movieTitle)

console.log('✌️movie see if the image is coming in or not--->', movie);
  return (
    <div>
      {/* <h1>{`this is the ${movieTitle}details page`}</h1> */}
      <div
  className="block  rounded-lg bg-white text-surface shadow-secondary-1 dark:bg-surface-dark dark:text-white border-2 border-black">
  <div className="relative overflow-hidden bg-cover bg-no-repeat h-80	 border-2 border-red">
    <img
      className="rounded-t-lg  w-full h-full object-cover "
      src={movie.image}
      alt="" />
  </div>
  <div className="p-6">
    <h1>{movie.movie}</h1>
    <p className="text-base">
      Some quick example text to build on the card title and make up the
      bulk of the card's content. Lorem ipsum dolor, sit amet consectetur adipisicing elit. Itaque est laboriosam delectus odio rem, amet fugiat repellendus explicabo pariatur saepe, natus architecto laudantium. Iste nobis aliquid maxime, iure et quae?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates quae numquam cumque distinctio. Pariatur, adipisci, laboriosam eveniet eum illo voluptate nulla similique labore possimus, ipsam quia quam aut ut. Quidem eveniet, dolores quod voluptas adipisci officia? Voluptatem fugit at ad quo, placeat neque facere inventore illum est odit, repellendus dolorem laborum voluptates provident, amet nihil voluptatum qui perspiciatis quas maxime voluptas. Perspiciatis qui corporis modi cum dolor dolore! Magni exercitationem corporis sint consequatur cupiditate voluptatibus fugiat et iure voluptate ipsum quidem earum repudiandae impedit fugit nemo debitis recusandae, blanditiis optio consectetur. Molestias, dolores illum dolorem aspernatur earum voluptatibus optio aliquam sit quaerat corrupti iusto animi temporibus ullam saepe tenetur maiores porro?
    </p>
  </div>
</div>
    </div>
  )
}

export default MovieDetails
