import React,{useState,useEffect} from 'react'
import MovieCard from './MovieCard'
import Pegination from './Pegination';

import { useDispatch, useSelector } from "react-redux";
import { fetchMovies } from '../features/moviesSlice';
import { Link,useSearchParams } from 'react-router-dom';



function MovieSection() {
  const dispatch = useDispatch();
  const moviesData = useSelector((state) => state.movies.moviesData);
console.log('✌️movies --->', moviesData);

  const [searchParams, setSearchParams] = useSearchParams();
  const[moviesPerPage,setMoviesPerPage]=useState(5)
  const[inputMovies,setInputMovies]=useState('')
  const [filterMovies,setFilteredMovies]=useState([])
  //const [currentPage, setCurrentPage] = useState(parseInt(searchParams.get('page')) || 1);
console.log('✌️filterMovies --->', filterMovies);

 useEffect(()=>{
  dispatch(fetchMovies())
 },[])


// const [currentPage, setCurrentPage] = useState(1);
const currentPage = parseInt(searchParams.get('page')) || 1;
// const moviesPerPage = 5;

const indexOfLastMovie = currentPage * moviesPerPage;
const indexOfFirstMovie = indexOfLastMovie - moviesPerPage;
const currentMovies = moviesData.slice(indexOfFirstMovie, indexOfLastMovie);
const currentSearchMovies = filterMovies.slice(indexOfFirstMovie, indexOfLastMovie);

const paginate = (pageNumber) => {
  setSearchParams({ page: pageNumber });
    // setCurrentPage(pageNumber);
  };

  const handleMoviesPerPageChange = (event) => {

    setMoviesPerPage(parseInt(event.target.value));

    // setCurrentPage(1); // Reset the current page when the movies per page changes
  };

  const handleInput=(event)=>{
    const input=event.target.value
    setInputMovies(input)
    const filteredMovies=moviesData.filter((movieObj)=>{
      return movieObj.movie.toLowerCase().includes(input)
    })
    if(!input){
      setFilteredMovies([])
    }
    else{
      setFilteredMovies(filteredMovies)
    }
  }

  return (
    <div className='py-0  '>
       <div >
        <input className='border-2 border-black' type="text" placeholder='enter movie name' onChange={handleInput} value={inputMovies} />
      </div>

     
      <div className="flex flex-wrap  px-2 mx-2 my-2 justify-center   ">

        <MovieCard moviesData={currentMovies} currentSearchMovies={currentSearchMovies} filterMovies={filterMovies} inputMovies={inputMovies}/>

      </div>

     {!inputMovies &&
       <Pegination
       moviesPerPage={moviesPerPage}
       totalMovies={moviesData.length}
       paginate={paginate}
     />

     }
      {inputMovies &&
       <Pegination
       moviesPerPage={moviesPerPage}
       totalMovies={filterMovies.length}
       paginate={paginate}
     />

     }
      
       <br />

    {/* <Link  to={'/api/movies/watchList'}><h1 className='inline-block'>My Watch List</h1></Link>
    <br /> */}

    <div className="flex justify-center mb-4">
        <label htmlFor="movies-per-page" className="mr-2">Movies per page:</label>
        <select
          id="movies-per-page"
          value={moviesPerPage}
          onChange={handleMoviesPerPageChange}
          className="border border-gray-300 rounded px-2 py-1"
        >
          <option value={5}>5</option>
          <option value={10}>10</option>
          <option value={15}>15</option>
          <option value={20}>20</option>
        </select>
      </div>
     

      {/* 
      <div className='flex flex-wrap  px-2 mx-2 my-2 justify-center '>
        {filterMovies.map((movies)=>{
          return(
           
        
              <div className="border-2 border-black  my-2 w-1/6 rounded-md h-full	  md:mr-4 lg:mr-6     inline-block">
                 <Link to={`/movies/${movies.movie}`}>
                  {movies.movie}
                </Link>
              </div>
             
       
          )
        })}
      </div> 
      */}
    </div>
    
  )
}

export default MovieSection
