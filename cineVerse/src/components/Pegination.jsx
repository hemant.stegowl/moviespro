import React from 'react'

function Pegination(props) {
    const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(props.totalMovies / props.moviesPerPage); i++) {
    pageNumbers.push(i);
  }
  return (
    <div className=' mt-12'>
       <nav className=' mt-6'>
      <ul className="pagination  flex justify-center">
        {pageNumbers.map((number) => (
          <li key={number} className={`page-item mr-4 border-2 w-8 border-black rounded-full pl-2 text-lg ${
            number === props.currentPage ? 'bg-blue text-white' : ''
          }`}>
            <button onClick={() => props.paginate(number)} className="page-link">
              {number}
            </button>
          </li>
        ))}
      </ul>
    </nav>
    </div>
  )
}

export default Pegination
