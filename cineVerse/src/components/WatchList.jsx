import React from 'react'
import { useDispatch,useSelector } from 'react-redux'
import { Link } from 'react-router-dom';
import { removeAddedMovies, removeFromWatchList,setAddedMovies } from '../features/moviesSlice';

function WatchList() {
    const dispatch=useDispatch()
    const addedMovies=useSelector((state)=>state.movies.addedMovies)
console.log('✌️addedMovies --->', addedMovies);
    const watchList=useSelector(state=>state.movies.watchList)
console.log('✌️watchList inside watch list component --->', watchList);

    const handleRemovingWatchList=(id,movie)=>{
        dispatch(removeFromWatchList(id))
//         const newAddedMovies=addedMovies.filter(one=>one.movie !== movie)
// console.log('✌️newAddedMovies --->', newAddedMovies);
        dispatch(removeAddedMovies(movie))
    }
  return (
    watchList.map(one=>{
        return(
            <div className='py-0 border-2 border-black h-32'>
               <h1>{one.movie}</h1>
               <p>know more on imdb <a href={one.imdb_url}>{one.imdb_url}</a></p>
               <button onClick={()=>{handleRemovingWatchList(one.id,one.movie)}}>remove</button>
            </div>
        )
     })
  )
}

export default WatchList
