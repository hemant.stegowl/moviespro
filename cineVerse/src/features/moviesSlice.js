import { createAsyncThunk,createSlice} from '@reduxjs/toolkit'
import axios from 'axios'

export const fetchMovies=createAsyncThunk('fetchMovies',async()=>{
    const response=await axios.get('https://dummyapi.online/api/movies?populate=*')
console.log('✌️response --->', response.data);
    return response.data
    // return response.data
})
export const moviesSlice=createSlice({
    name:'movies',
    initialState:{
        moviesData:[],
        watchList:[],
        addedMovies:[]
    },
    reducers:{
        setWatchList:(state,action)=>{
            state.watchList=action.payload
        },

        removeFromWatchList: (state, action) => {
            // Filter out the movie with the specified ID from the watchList
            state.watchList = state.watchList.filter(movie => movie.id !== action.payload);
        },

        setAddedMovies:(state,action)=>{
            state.addedMovies=action.payload
        },

        removeAddedMovies:(state,action)=>{
            state.addedMovies=state.addedMovies.filter(one=>one !== action.payload)
        }

    },
    extraReducers: (builder) => {
        builder
        .addCase(fetchMovies.pending, (state, action) => {
            console.log('pending data');
          })
          .addCase(fetchMovies.fulfilled, (state, action) => {
            // const arrAnecdote = action.payload.map(one => one.text);
            state.moviesData = action.payload;
          })        
          .addCase(fetchMovies.rejected, (state, action) => {
            console.log('error', action.payload);
          })
         
      }
    
})
export const {setWatchList,removeFromWatchList,setAddedMovies,removeAddedMovies}=moviesSlice.actions;

export default moviesSlice.reducer;

// moviesData:[
//     {
//         imgUrl:"https://plus.unsplash.com/premium_photo-1661849963038-7a735f000ae0?q=80&w=1830&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
//         title:'Diver',
//         id:1
//     },
//     {
//         imgUrl:"https://m.media-amazon.com/images/M/MV5BNTkyOGVjMGEtNmQzZi00NzFlLTlhOWQtODYyMDc2ZGJmYzFhXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_.jpg",
//         title:'3 idiot',
//         id:2
//     },
//     {
//         imgUrl:"https://images-us.bookshop.org/ingram/9780451478290.jpg?height=500&v=v2",
//         title:'13 reasons why',
//         id:3
//     },
//     {
//         imgUrl:"https://alternativemovieposters.com/wp-content/uploads/2021/06/HaleyTurnbull_TokyoDrift.jpg",
//         title:'tokyo drift',
//         id:4
//     },
//     {
//         imgUrl:"https://rukminim2.flixcart.com/image/850/1000/k8xduvk0/poster/j/m/z/medium-the-dark-knight-poster-decorative-wall-poster-wall-d-cor-original-imafqu8evqxuvfvg.jpeg?q=90&crop=false",
//         title:'the dark knight',
//         id:5
//     },
//     {
//       imgUrl:"https://m.media-amazon.com/images/M/MV5BNTkyOGVjMGEtNmQzZi00NzFlLTlhOWQtODYyMDc2ZGJmYzFhXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_.jpg",
//       title:'3 idiot',
//       id:2
//   },
//   {
//     imgUrl:"https://m.media-amazon.com/images/M/MV5BNTkyOGVjMGEtNmQzZi00NzFlLTlhOWQtODYyMDc2ZGJmYzFhXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_.jpg",
//     title:'3 idiot',
//     id:2
//   },
    
//   ]