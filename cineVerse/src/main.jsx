import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import { Route, RouterProvider, createBrowserRouter, createRoutesFromElements} from 'react-router-dom'
import Layout from './Layout.jsx'
import MovieSection from './components/MovieSection.jsx'
import MovieDetails from './components/MovieDetails.jsx'
import store from './redux/store.js'
import { Provider } from 'react-redux'
import WatchList from './components/WatchList.jsx'
import Homie from './pages/Homie.jsx'
const moviesData=[
  {
      imgUrl:"https://plus.unsplash.com/premium_photo-1661849963038-7a735f000ae0?q=80&w=1830&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
      title:'Diver',
      id:1
  },
  {
      imgUrl:"https://m.media-amazon.com/images/M/MV5BNTkyOGVjMGEtNmQzZi00NzFlLTlhOWQtODYyMDc2ZGJmYzFhXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_.jpg",
      title:'3 idiot',
      id:2
  },
  {
      imgUrl:"https://images-us.bookshop.org/ingram/9780451478290.jpg?height=500&v=v2",
      title:'13 reasons why',
      id:3
  },
  {
      imgUrl:"https://alternativemovieposters.com/wp-content/uploads/2021/06/HaleyTurnbull_TokyoDrift.jpg",
      title:'tokyo drift',
      id:4
  },
  {
      imgUrl:"https://rukminim2.flixcart.com/image/850/1000/k8xduvk0/poster/j/m/z/medium-the-dark-knight-poster-decorative-wall-poster-wall-d-cor-original-imafqu8evqxuvfvg.jpeg?q=90&crop=false",
      title:'the dark knight',
      id:5
  },
  {
    imgUrl:"https://m.media-amazon.com/images/M/MV5BNTkyOGVjMGEtNmQzZi00NzFlLTlhOWQtODYyMDc2ZGJmYzFhXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_.jpg",
    title:'3 idiot',
    id:2
},
{
  imgUrl:"https://m.media-amazon.com/images/M/MV5BNTkyOGVjMGEtNmQzZi00NzFlLTlhOWQtODYyMDc2ZGJmYzFhXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_.jpg",
  title:'3 idiot',
  id:2
},
  
]

const router=createBrowserRouter(
  createRoutesFromElements(
   <>
   <Route path='/' element={<Homie/>}/>
     <Route path='/movies' element={<Layout />}>
      <Route path='' element={<MovieSection/>}/>
      <Route path='/movies/:movieTitle' element={<MovieDetails />}/>
      <Route path='/movies/watchList' element={<WatchList/>}/>
    </Route>
   </>
   
  )
)

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
  <Provider store={store}>
    <RouterProvider router={router}/>
  </Provider>
  </React.StrictMode>,
)
