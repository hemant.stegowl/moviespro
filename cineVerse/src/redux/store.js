import { configureStore } from '@reduxjs/toolkit'
//import anecdoteReducer from '../features/counter/anecdoteSlice'
//import anecdoteSlice from '../features/counter/anecdoteSlice'
import moviesSlice from '../features/moviesSlice'

export default configureStore({
  reducer: {
    movies:moviesSlice,
  },
})